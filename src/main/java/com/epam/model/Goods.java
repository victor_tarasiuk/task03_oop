package com.epam.model;

/**
 * @author Victor Tarasiuk
 * @version 1.0
 * @since 11.11.2019
 *
 */
public class Goods {
    private String goodsName;
    private double goodsPrice;
    private GoodsType goodsType;

    public Goods(final String goodsName, final double goodsPrice,
                 final GoodsType goodsType) {
        this.goodsName = goodsName;
        this.goodsPrice = goodsPrice;
        this.goodsType = goodsType;
    }

    public GoodsType getGoodsType() {
        return goodsType;
    }

    public void setGoodsType(final GoodsType goodsType) {

        this.goodsType = goodsType;
    }

    public String getGoodsName() {

        return goodsName;
    }

    public void setGoodsName(final String goodsName) {

        this.goodsName = goodsName;
    }

    public double getGoodsPrice() {

        return goodsPrice;
    }

    public void setGoodsPrice(final double goodsPrice) {

        this.goodsPrice = goodsPrice;
    }

    @Override
    public String toString() {
        return goodsName + ": price = " + goodsPrice + "$";
    }
}
