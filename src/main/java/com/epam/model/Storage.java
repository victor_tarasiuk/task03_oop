package com.epam.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import static com.epam.model.GoodsType.*;

/**
 * @author Victor Tarasiuk
 * @version 1.0
 * @since 11.11.2019
 *
 */
public class Storage {
    public static final double PRICE_VACUUM_CLEANER = 75.10;
    public static final double PRICE_HAIR_DRYER = 14.45;
    public static final double PRICE_RAZOR = 20.65;
    public static final double PRICE_TV = 300.50;
    public static final double PRICE_CD_PLAYER = 50.30;
    public static final double PRICE_HAMMER = 5.50;
    public static final double PRICE_SCREWDRIVER = 2.40;
    public static final double PRICE_SCISSORS = 1.70;
    public static final double PRICE_CHISEL = 0.90;
    public static final double PRICE_KNIFE = 4.20;

    private Set<Goods> goodsStorage;

    public Storage() {
        goodsStorage = new HashSet<>();

        Goods vacuumCleaner = new Goods("Vacuum cleaner",
                PRICE_VACUUM_CLEANER, ELECTRICALHOUSEHOLDGOODS);
        Goods hairDryer = new Goods("Hair dryer",
                PRICE_HAIR_DRYER, ELECTRICALHOUSEHOLDGOODS);
        Goods razor = new Goods("Razor",
                PRICE_RAZOR, ELECTRICALHOUSEHOLDGOODS);
        Goods tv = new Goods("TV",
                PRICE_TV, ELECTRICALHOUSEHOLDGOODS);
        Goods cdPlayer = new Goods("CD Player",
                PRICE_CD_PLAYER, ELECTRICALHOUSEHOLDGOODS);

        Goods hammer = new Goods("Hammer",
                PRICE_HAMMER, HANDTOOLS);
        Goods screwdriver = new Goods("Screwdriver",
                PRICE_SCREWDRIVER, HANDTOOLS);
        Goods scissors = new Goods("Scissors",
                PRICE_SCISSORS, HANDTOOLS);
        Goods chisel = new Goods("Chisel",
                PRICE_CHISEL, HANDTOOLS);
        Goods knife = new Goods("Knife",
                PRICE_KNIFE, HANDTOOLS);

        /**
         * Add goods to collection.
         */
        goodsStorage.add(vacuumCleaner);
        goodsStorage.add(hairDryer);
        goodsStorage.add(razor);
        goodsStorage.add(tv);
        goodsStorage.add(cdPlayer);
        goodsStorage.add(hammer);
        goodsStorage.add(screwdriver);
        goodsStorage.add(scissors);
        goodsStorage.add(chisel);
        goodsStorage.add(knife);
    }

    /**
     * Created method get goods list.
     */
    public final Set<Goods> getGoodsStorage() {

        return goodsStorage;
    }


    /**
     * Created method find goods cheaper.
     */
    public final List<Goods> findGoodsCheaper(final double priceWanted) {

        List<Goods> sortedByPriceList = new ArrayList<>();

        for (Goods g : goodsStorage) {

            if (g.getGoodsPrice() < priceWanted) {
                sortedByPriceList.add(g);
            }
        }

        return sortedByPriceList;
    }

    /**
     * Created method get goods by category.
     */
    public final List<Goods> getAllGoodsList(final String category) {
        List<Goods> goodsByCategoryList = new ArrayList<>();

        for (Goods g: getGoodsStorage()) {

            if (g.getGoodsType().getName().equalsIgnoreCase(category)) {
                goodsByCategoryList.add(g);
            }
        }

        return goodsByCategoryList;
    }


}
