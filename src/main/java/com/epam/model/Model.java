package com.epam.model;

import java.util.List;

/**
 * @author Victor Tarasiuk
 * @version 1.0
 * @since 11.11.2019
 *
 */
public interface Model {

    /**
     * Will get goods list.
     */
    List<Goods> getAllGoodsList();

    /**
     * Will find goods cheaper.
     */
    List<Goods> findGoodsCheaper(double priceWanted);

    /**
     * Will get goods by category.
     */
    List<Goods> getGoodsByCategory(String category);
}
