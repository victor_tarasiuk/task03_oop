package com.epam.model;

/**
 * @author Victor Tarasiuk
 * @version 1.0
 * @since 11.11.2019
 *
 */
public enum GoodsType {
    ELECTRICALHOUSEHOLDGOODS("Electrical household goods"),
    HANDTOOLS("Hand tools goods");

    private final String name;

     GoodsType(final String name) {
        this.name = name;
    }

    public String getName() {

         return name;
    }
}
