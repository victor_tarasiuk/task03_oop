package com.epam.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Victor Tarasiuk
 * @version 1.0
 * @since 11.11.2019
 *
 */
public class BusinessLogic implements Model {

    private Storage storage;

    public BusinessLogic() {
        storage = new Storage();
    }


    /**
     * Created method generate  get all goods list.
     */
    @Override
    public final List<Goods> getAllGoodsList() {
        return new ArrayList<>(storage.getGoodsStorage());
    }

    /**
     * Created method generate  find goods cheaper.
     */
    @Override
    public final List<Goods> findGoodsCheaper(final double priceWanted) {
        return storage.findGoodsCheaper(priceWanted);
    }

    /**
     * Created method generate  get goods by category.
     */
    @Override
    public final List<Goods> getGoodsByCategory(final String category) {
        return new ArrayList<>(storage.getAllGoodsList(category));
    }
}