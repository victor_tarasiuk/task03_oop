package com.epam.view;

/**
 * @author Victor Tarasiuk
 * @version 1.0
 * @since 11.11.2019
 *
 */
public interface View {

    /**
     * Will show what user should do and display information.
     */
    void show();

}
