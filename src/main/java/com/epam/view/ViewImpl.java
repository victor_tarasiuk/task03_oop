package com.epam.view;

import com.epam.controller.ControllerImpl;
import java.util.Scanner;

/**
 * @author Victor Tarasiuk
 * @version 1.0
 * @since 11.11.2019
 *
 */
public class ViewImpl implements View {
    /**
     * Creates object ControllerImpl to execute methods.
     */
    ControllerImpl controller = new ControllerImpl();

    /**
     * Created Scanner to set numbers.
     */
    Scanner scanner = new Scanner(System.in);

    /**
     * Created method to show what user should do and display information.
     */
    public final void show() {

        while (true) {
            System.out.println("Get list of all goods in the hypermarket: 1");
            System.out.println("Get list of goods less than set number: 2");
            System.out.println("Get list of goods by category: 3");

            String string = scanner.nextLine();

            switch (string) {

                case "1":
                    System.out.println(controller.generateGoodsList());
                    break;

                case "2":
                    System.out.println("Enter desired price: ");
                    double priceWanted = Double.parseDouble(scanner.nextLine());
                    System.out.println(controller.
                            generateGoodsLessPrice(priceWanted));
                    break;

                case "3":
                    System.out.println("Categories: " +
                            "Electrical household goods, Hand tools goods");
                    System.out.println("Enter desired category: ");
                    String category = scanner.nextLine();
                    System.out.println(controller.generateGoodsByCategory(category));
                    break;

                default: return;
            }
        }

    }

}
