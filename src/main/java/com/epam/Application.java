package com.epam;

import com.epam.view.ViewImpl;

/**
 * @author Victor Tarasiuk
 * @version 1.0
 * @since 11.11.2019
 *
 */
public class Application {
    public static void main(String[] args) {

        /**
         * Creates object ViewImp and insert method to execute.
         */
        ViewImpl view = new ViewImpl();
        view.show();

    }
}
