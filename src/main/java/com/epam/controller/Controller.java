package com.epam.controller;

import com.epam.model.Goods;
import java.util.List;

/**
 * @author Victor Tarasiuk
 * @version 1.0
 * @since 11.11.2019
 *
 */
public interface Controller {

    /**
     * Will generate goods list.
     */
    List<Goods> generateGoodsList();

    /**
     * Will generate goods less price.
     */
    List<Goods> generateGoodsLessPrice(double price);

    /**
     * Will generate goods by category.
     */
    List<Goods> generateGoodsByCategory(String category);
}
