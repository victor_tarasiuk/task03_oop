package com.epam.controller;

import com.epam.model.BusinessLogic;
import com.epam.model.Goods;
import com.epam.model.Model;
import java.util.List;

/**
 * @author Victor Tarasiuk
 * @version 1.0
 * @since 11.11.2019
 *
 */

public class ControllerImpl implements Controller {

    private Model model;

    public ControllerImpl() {
        model = new BusinessLogic();
    }

    /**
     * Created method generate goods list.
     */
    @Override
    public final List<Goods> generateGoodsList() {
        return model.getAllGoodsList();
    }

    /**
     * Created method generate goods less price.
     */
    @Override
    public final List<Goods> generateGoodsLessPrice(final double price) {
        return model.findGoodsCheaper(price);
    }

    /**
     * Created method generate goods by category.
     */
    @Override
    public final List<Goods> generateGoodsByCategory(final String category) {
        return model.getGoodsByCategory(category);
    }
}
